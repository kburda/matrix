#include <iostream>

#include "unit_tests.h"

int main()
{
    run_all_tests();

    return 0;
}
