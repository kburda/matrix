#include "matrix.h"

#include <iostream>
#include <stdexcept>

namespace burda {

    /**
    Constructor
    @param name - symbolic name of the matrix (string)
    @param num of rows
    @param num of cols
    */
    template <typename T>
    Matrix<T>::Matrix(const std::string p_name, size_t p_rows, size_t p_cols)
    {
        this->name = p_name;
        this->rows = p_rows;
        this->cols = p_cols;
        this->values = new T*[this->rows];

        // basic check
        if (this->values != nullptr)
        {
            for (size_t i=0; i<rows; i++)
            {
                this->values[i] = new T[cols];
            }
        }
        else
        {
            throw std::runtime_error ("Cannot allocate values");
        }

        this->fill_with_zeros();
    }

    /**
    Destruction
    */
    template <typename T>
    Matrix<T>::~Matrix()
    {
        this->free_array();
    }

    /**
    Prints the matrix to the stdout
    */
    template <typename T>
    void Matrix<T>::print() const
    {
        for (size_t i=0; i<rows; i++)
        {
            for (size_t j=0; j<cols; j++)
            {
                std::cout << this->values[i][j];

                if (j < (cols-1))
                {
                    std::cout << ",";
                }
            }
            std::cout << std::endl;
        }
    }

    /**
    Simple getter
    @returns inner values as 2D array
    */
    template <typename T>
    T ** Matrix<T>::get_values() const
    {
        return this->values;
    }

    /**
    Operator in order to approach inner values.
    E.g. for the very first element: my_matrix(0,0) = 1;
    */
    template <typename T>
    T& Matrix<T>::operator() (size_t p_idx_row, size_t p_idx_col)
    {
        // assert bounds
        if (p_idx_row >= this->rows || p_idx_col >= this->cols)
        {
            throw std::out_of_range("Idx out of range");
        }

        return this->values[p_idx_row][p_idx_col];
    }

    /**
    Assignment operator
    */
    template <typename T>
    const Matrix<T>& Matrix<T>::operator=(const Matrix<T>& p_other)
    {
        // basic check
        if (this != &p_other) {
            // at first, release actual array values
            this->free_array();

            this->name = p_other.name;
            this->rows = p_other.rows;
            this->cols = p_other.cols;

            // there might be a different size of arrays
            this->values = new T*[this->rows];
            if (this->values != nullptr) {
                for (size_t i=0; i<this->rows; i++)
                {
                    this->values[i] = new T[this->cols];
                }
            }
            else
            {
                throw std::runtime_error ("Cannot allocate values");
            }

            // copy the values themselves
            for (size_t i=0; i<this->rows; i++)
                {
                for (size_t j=0; j<this->cols; j++)
                {
                    this->values[i][j] = p_other.values[i][j];
                }
            }
        }

        return *this;
    }

    /**
    Copy consturctor
    */
    template <typename T>
    Matrix<T>::Matrix(const Matrix<T> &p_other)
    {
        *this = p_other;
        this->name = p_other.name + "_copy";
    }

    /**
    Move constructor
    */
    template <typename T>
    Matrix<T>::Matrix(Matrix<T>&& p_other)
    {
        *this = std::move (p_other);
    }

    /**
    Move operator
    */
    template <typename T>
    Matrix<T>& Matrix<T>::operator=(Matrix<T>&& p_other)
    {
        *this = p_other;

        return *this;
    }

    /**
    Plus operator
    */
    template <typename T>
    Matrix<T> Matrix<T>::operator+(const Matrix<T>& p_other)
    {
        // assert bounds
        if (this->rows != p_other.get_num_rows() || this->cols != p_other.get_num_cols())
        {
            throw new std::out_of_range("out of range");
        }

        Matrix v_ret ("ret+", this->rows, this->cols);

        for (size_t i=0; i<this->rows; i++)
        {
            for (size_t j=0; j<this->cols; j++)
            {
                v_ret.values[i][j] = this->values[i][j] + p_other.values[i][j];
            }
        }

        return v_ret;
    }

    /**
    Multiplication operator
    */
    template <typename T>
    Matrix<T> Matrix<T>::operator*(const Matrix<T>& p_other)
    {
        Matrix v_ret ("ret*", this->rows, p_other.get_num_cols());

        // assertion
        if (this->cols != p_other.get_num_rows())
        {
            throw new std::out_of_range("out of range");
        }

        for (size_t i=0; i<v_ret.get_num_rows(); i++)
            {
            for (size_t j=0; j<v_ret.get_num_cols(); j++)
            {
                // calculate the sum
                T v_sum = 0;
                for (size_t k=0; k<this->get_num_cols(); k++)
                {
                    v_sum += (this->values[i][k] * p_other.values[k][j]);
                }
                v_ret.values[i][j] += v_sum;
            }
        }

        return v_ret;
    }

    /**
    Helper method to free the inner values
    */
    template <typename T>
    void Matrix<T>::free_array()
    {
        // we have to do this check
        if (this->rows == 0 && this->cols == 0)
        {
            return;
        }

        // free columns
        for (size_t i=0; i<rows; i++)
        {
            delete[] this->values[i];
        }

        // free rows
        delete[] this->values;
    }

    /**
    Helper to nullify
    */
    template <typename T>
    void Matrix<T>::fill_with_zeros()
    {
        for (size_t i=0; i<this->rows; i++)
        {
            for (size_t j=0; j<this->cols; j++)
            {
                this->values[i][j] = 0;
            }
        }
    }

    /**
    Getter for num of rows
    */
    template <typename T>
    size_t Matrix<T>::get_num_rows() const
    {
        return this->rows;
    }

    /**
    Getter for num of cols
    */
    template <typename T>
    size_t Matrix<T>::get_num_cols() const
    {
        return this->cols;
    }

    /**
    The explicit instantiation part.
    Here we force to compile template with the basic numeric type.
    */
    template class Matrix<int>;
};
