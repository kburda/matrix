#include "unit_tests.h"

#include <iostream>

#include "matrix.h"


/**
Global vars needed for tests
*/
// allocate on the heap
burda::Matrix<int> * g_matrix = nullptr;
// allocate on the stack
burda::Matrix<int> g_matrix_stack("g_matrix_stack", 4, 4);

/**
Helper method for logging
*/
void log(const std::string& p_str)
{
    std::clog << p_str << std::endl;
}

/**
Tests creation of matrices
*/
bool test_creation()
{
    log("test_creation*****");

    bool v_ret = true;
    g_matrix = nullptr;

    // try to allocate new matrix
    try
    {
        g_matrix = new burda::Matrix<int>("g_matrix", 2, 2);
    }
    catch (std::exception& e)
    {
        return false;
    }

    return v_ret;
}

/**
Tests matrix inner values
*/
bool test_vals()
{
    log("test_vals*****");

    bool v_ret = true;

    // set some values
    try
    {
        g_matrix->get_values()[0][0] = 50;
        g_matrix->get_values()[0][1] = 1;

        // assert them
        if (g_matrix->get_values()[0][0] != 50 || g_matrix->get_values()[0][1] != 1)
        {
            v_ret = false;
        }
    }
    catch (std::exception& e)
    {
        return false;
    }

    return v_ret;
}

/**
Tests operators
*/
bool test_operators()
{
    log("test_operators*****");

    bool v_ret = true;
    burda::Matrix<int> * m = nullptr;

    try
    {
        // subscript test
        g_matrix->get_values()[1][1] = 10;

        if ((*g_matrix)(1, 1) != 10)
        {
            v_ret = false;
        }

        // test copy constructor
        g_matrix_stack(0, 0) = 1;
        g_matrix_stack(0, 1) = 1;
        burda::Matrix<int> v_matrix_stack_cpy (g_matrix_stack);
        if (v_matrix_stack_cpy(0, 0) != g_matrix_stack(0, 0))
        {
            v_ret = false;
        }

        // test assignment operator
        burda::Matrix<int> v_matrix_stack_cpy2("v_matrix_stack_cpy2", 5, 5);
        v_matrix_stack_cpy2 = g_matrix_stack;
        if (v_matrix_stack_cpy2(0, 1) != g_matrix_stack(0, 1))
        {
            v_ret = false;
        }

        m = new burda::Matrix<int>("m", 10, 10);
        burda::Matrix<int> * other = m;
        other->get_num_cols();
        delete m;
        m = 0;
    }
    catch (std::exception& e)
    {
        delete m;

        return false;
    }

    return v_ret;
}

/**
Test numeric operators
*/
bool test_numeric()
{
    log("test_numeric*****");

    bool v_ret = true;
    burda::Matrix<int> * v_result = nullptr;

    try
    {
        // test + operator
        burda::Matrix<int> v_copy_1 (*g_matrix);
        burda::Matrix<int> v_copy_2 (*g_matrix);
        burda::Matrix<int> v_test ("res", v_copy_1.get_num_rows(), v_copy_2.get_num_cols());
        v_copy_1(0, 0) = 50;
        v_copy_2(0, 0) = 60;

        // working with new matrix
        v_result = new burda::Matrix<int>("add", v_copy_1.get_num_rows(), v_copy_2.get_num_cols());
        (*v_result) = (v_copy_1 + v_copy_2);
        v_test = (v_copy_1 + v_copy_2);

        // assert results
        if (v_test(0, 0) != 110 || (*v_result)(0, 0) != 110)
        {
            v_ret = false;
        }

        delete v_result;
        v_result = 0;

        // test * operator
        burda::Matrix<int> v_A ("A", 2, 4);
        burda::Matrix<int> v_B ("B", 4, 3);
        v_A(0, 0) = 2;
        v_A(0, 1) = 5;
        v_A(0, 2) = 3;
        v_A(0, 3) = -1;
        v_A(1, 0) = -3;
        v_A(1, 1) = 4;
        v_A(1, 2) = 0;
        v_A(1, 3) = 1;

        v_B(0, 0) = -2;
        v_B(0, 1) = 2;
        v_B(0, 2) = 3;
        v_B(1, 0) = -4;
        v_B(1, 1) = 4;
        v_B(1, 2) = 5;
        v_B(2, 0) = -3;
        v_B(2, 1) = 0;
        v_B(2, 2) = 6;
        v_B(3, 0) = 7;
        v_B(3, 1) = 8;
        v_B(3, 2) = 9;

        burda::Matrix<int> v_res("(A+B)", v_A.get_num_rows(), v_B.get_num_cols());
        v_res = v_A * v_B;

        if (v_res(0, 0) != (-40) || v_res(1, 2) != 20)
        {
            v_ret = false;
        }
    }
    catch (std::exception& e)
    {
        delete v_result;

        return false;
    }

    return v_ret;
}

/**
Test std::move
*/
bool test_move()
{
    log("test_move*****");

    bool v_ret = true;

    try
    {
        burda::Matrix<int> v_A("A", 2, 2);
        burda::Matrix<int> v_B("B", 2, 2);

        v_B(0, 0) = 100;

        v_A = std::move (v_B);

        if (v_A(0, 0) != 100)
        {
            v_ret = false;
        }

        burda::Matrix<int> v_moved (std::move(v_A));

        if (v_moved(0, 0) != v_A(0, 0))
        {
            v_ret = false;
        }
    }
    catch (std::exception& e)
    {
        return false;
    }

    return v_ret;
}

/**
Clean-up after the testing
*/
void global_tear_down()
{
    log("Tear down");

    delete g_matrix;
    g_matrix = 0;
}

/**
Wrapper to run all specified tests
*/
void run_all_tests()
{
    log("Running all tests");
    bool v_is_created = test_creation();

    log(" ----------> " + std::to_string(v_is_created));

    if (v_is_created)
    {
        log(" ----------> " + std::to_string(test_vals()));
        log(" ----------> " + std::to_string(test_operators()));
        log(" ----------> " + std::to_string(test_numeric()));
        log(" ----------> " + std::to_string(test_move()));
    }

    global_tear_down();
}
