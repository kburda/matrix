#ifndef MATRIX_H
#define MATRIX_H

/**
Karel Burda, 2011
*/

#include <string>

namespace burda
{

    /**
    Class represents abstract 2-dimensional matrix.
    Written in the upcoming C++ 11.
    Deliberately not using std smart pointers
    */
    template <typename T>
    class Matrix
    {
        public:

            // ctors, dtors
            Matrix() = delete;
            Matrix(const std::string p_name = "", size_t p_rows = 0, size_t p_cols = 0);
            virtual ~Matrix();

            // public methods
            virtual void print() const;

            // getters
            T ** get_values() const;
            size_t get_num_rows() const;
            size_t get_num_cols() const;

            // operators
            T& operator() (size_t p_idx_row, size_t p_idx_col);
            Matrix<T> (const Matrix<T> &p_other);
            Matrix<T> (Matrix<T>&& p_other);
            const Matrix<T>& operator=(const Matrix<T>& p_other);
            Matrix<T> operator+(const Matrix<T>& p_other);
            Matrix<T> operator*(const Matrix<T>& p_other);
            Matrix<T>& operator=(Matrix<T>&& p_other);

        protected:

            void fill_with_zeros();
            void free_array();

            T ** values = nullptr;

            std::string name = "";
            size_t rows = 0;
            size_t cols = 0;
    };
};

#endif // MATRIX_H
